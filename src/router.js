"use strict";

import Vue from "vue";
import VueRouter from "vue-router";
import LoginPage from "./components/login_register/LoginPage";
import RegisterPage from "./components/login_register/RegisterPage";
import WritePage from "./components/write/WritePage";
import ProjectPage from "./components/project/ProjectPage";
import ProjectPage2 from "./components/project/ProjectPage2";
import UserPage from "./components/user/UserPage";
import ChatPage from "./components/chat/ChatPage";
import ProjectFilePage from "./components/projectfile/ProjectFilePage";

Vue.use(VueRouter);

const routes = [
    { path: "/", name: "LoginPage", component: LoginPage },
    { path: "/write", name: "WritePage", component: WritePage },
    { path: "/register", name: "RegisterPage", component: RegisterPage },
    { path: "/project", name: "ProjectPage", component: ProjectPage },
    { path: "/project2", name: "ProjectPage2", component: ProjectPage2 },
    { path: "/user", name: "UserPage", component: UserPage },
    { path: "/chat", name: "ChatPage", component: ChatPage },
    { path: "/file", name: "ProjectFilePage", component: ProjectFilePage },
];

const router = new VueRouter({
    mode: "history",
    routes,
    scrollBehavior(to, from, savedPosition) {
        //回退时保持滚动条为回退前的状态
        if (savedPosition) {
            return savedPosition
        } else {
            return { x: 0, y: 0 }
        }
    }
});

router.beforeEach((to, from, next) => {
    if (to.matched.some(res => res.meta.requireAuth)) {// 判断是否需要登录权限
        if (sessionStorage.getItem("userName")) {// 判断是否登录
            next()
        } else {// 没登录则跳转到登录界面
            next({
                path: "/",
                query: { redirect: to.fullPath }
            })
        }
    } else {
        next()
    }
});

export default router;
