"use strict";

import Vue from "vue";
import Vuex from "vuex";
import * as getters from "./getters";
import * as mutations from "./mutations";
import * as actions from "./actions";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        token: "",
        //currentUser: null,
        isLogin: false,
        userName: "",
        nickName: "",
        userAvatar: null,
        userColor: "",
        projectName: "",
    },
    getters,
    mutations,
    actions,
    // getters: {
    //     getUsername(state) {
    //         return state.userName;
    //     },
    //     getAvatar(state) {
    //         return state.userAvatar;
    //     },
    //     getName(state) {
    //         return state.nickName;
    //     },
    //     getColor(state) {
    //         return state.userColor;
    //     },
    //     getProjectName(state) {
    //         return state.projectName;
    //     },
    // },
    // mutations: {
    //     setToken(state, token) {
    //         state.token = token;
    //     },
    //     setUsername(state, userName) {
    //         state.userName = userName;
    //     },
    //     setUserAvatar(state, avatar) {
    //         state.userAvatar = avatar;
    //     },
    //     setUserColor(state, avatar) {
    //         state.userColor = avatar;
    //     },
    //     setNickName(state, avatar) {
    //         state.nickName = avatar;
    //     },
    //     setProjectName(state, pn) {
    //         state.projectName = pn;
    //     },
    // },
});
