"use strict";

//export const currentUser = state => state.currentUser;
export const userName = state => state.userName;
export const userAvatar = state => state.userAvatar;
export const nickName = state => state.nickName;
export const userColor = state => state.userColor;
export const projectName = state => state.projectName;
export const isLogin = state => state.isLogin;
