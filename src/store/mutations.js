"use strict";

//更改用户状态信息
export const userStatus = (state, user) => {
    if (!user) {
        //登出的时候，清空sessionStorage里的东西
        sessionStorage.setItem("userToken", "");
        sessionStorage.setItem("userName", null);
        sessionStorage.setItem("nickName", null);
        sessionStorage.setItem("userAvatar", null);
        sessionStorage.setItem("userColor", null);

        //state.currentUser = null;
        state.isLogin = false;
        state.token = "";
        state.userName = "";
        state.nickName = "";
        state.userColor = "";
        state.userAvatar = null;
        state.projectName = "";
        return;
    }

    //state.currentUser = user;
    state.userName = user.userName;
    state.nickName = user.nickName;
    state.userAvatar = user.avatar;
    state.userColor = user.color;
    state.isLogin = true;
};

export const userToken = (state, userToken) => {
    if (!userToken) {
        state.token = null;
        return;
    }
    state.token = userToken;
};

export const userAvatar = (state, userAvatar) => {
    if (!userAvatar) {
        state.userAvatar = 'mdi-fish';
        return;
    }

    state.userAvatar = userAvatar;
};

export const userName = (state, userName) => {
    if (!userName) {
        state.userName = null;
        return;
    }
    state.userName = userName;
};

export const nickName = (state, nickName) => {
    if (!nickName) {
        state.nickName = "默认用户";
        return;
    }
    state.nickName = nickName;
};

export const userColor = (state, userColor) => {
    if (!userColor) {
        state.userColor = "blue";
        return;
    }
    state.userColor = userColor;
};

export const projectName = (state, projectName) => {
    if (!projectName) {
        state.projectName = projectName;
        return;
    }
    state.projectName = projectName;
};
