"use strict";

class SiteConfig {
    constructor() {
        this.Base_Url = "localhost";
        this.WWWPort = 3000;
        this.APIPort = 3000;
    }

    wwwUrl() {
        return `http://${this.Base_Url}:${this.WWWPort}`;
    }
    apiUrl() {
        return `http://${this.Base_Url}:${this.APIPort}`;
    }
}

const siteConfig = new SiteConfig();
export default siteConfig;