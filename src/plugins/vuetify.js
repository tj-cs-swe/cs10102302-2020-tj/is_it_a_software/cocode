import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: '#f57ce1',
        secondary: '#b4fdff',
        thirdary: '#dd23be', 
        accent: '#00d7df',
        error: '#b71c1c',
        success: '#9affbc',
        whitee: '#f9fafd'
      }
    }
  }
});
