"use strict";

import Vue from "vue";
import store from "./store";
import App from "./App.vue";
import vuetify from "./plugins/vuetify";
import router from "./router";
import axios from "axios";
//import ClientSocketIO from "socket.io-client";
//import ServerSocketIO from "vue-socket.io";
import siteConfig from "./siteConfig";
import EventBus from "./eventBus";
import SocketIO from "./socketio";

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

// 注入axios全局访问入口
axios.defaults.baseURL = siteConfig.apiUrl();
Vue.prototype.$axios = axios;
// 注入站点全局变量
Vue.prototype.$siteConfig = siteConfig;
// 注入eventbus全局入口
Vue.prototype.$evtbus = EventBus;
// 注入socketio全局入口
Vue.prototype.$socketio = new SocketIO(siteConfig.apiUrl(), EventBus);

Vue.config.productionTip = false;

Vue.use(ElementUI);

//路由全局守卫
// const store1 = {
//     state: {
//         token: sessionStorage.getItem('userToken') || '',
//     },
// }

// http请求拦截器
axios.interceptors.request.use(
    function(config) {
        if (store.state.token) {
            // 判断是否存在token，如果存在的话，则每个http header都加上token
            config.headers.Authorization = `Bearer ${store.state.token}`;
            // config.headers.Authorization = token +Bearer;
        }
        return config;
        // 给视频添加评论提交之前对评论进行判断
    },
    function(error) {
        return Promise.reject(error);
    },
);

//http 响应拦截器
axios.interceptors.response.use(
    response => {
        return response;
    },
    error => {
        if (error.response) {
            switch (error.response.status) {
                case 401:
                    // 这里写清除token的代码
                    sessionStorage.setItem("userToken", null);
                    sessionStorage.setItem("userName", null);
                    sessionStorage.setItem("userAvatar", null);
                    console.log(router.currentRoute.fullPath);
                    router.replace({
                        path: "/",
                        query: { redirect: router.currentRoute.fullPath }, //登录成功后跳入浏览的当前页面
                    });
            }
        }
        return Promise.reject(error.response);
    },
);

//Vue.use(new ServerSocketIO({
//  debug: true,
//  connection: ClientSocketIO(`http://${siteConfig.wwwUrl()}/`),
//}));

new Vue({
    vuetify,
    router,
    store,
    el: '#app',
    render: h => h(App),
}).$mount("#app");
