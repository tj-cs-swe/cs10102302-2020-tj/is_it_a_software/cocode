"use strict";

//注册(还未处理用户名已存在等问题)
const express = require("express");
const router = express.Router();
const { User } = require("../db/db");

/* register */
router.post("/", async(req, res) => {
    console.log(req.body);

    const user = await User.findOne({
        username: req.body.userName,
    });

    if (!user) {
        res.send({
            status: "UPDATE_ERROR",
            message: "用户不存在",
        });
        return;
    }

    user.nickName = req.body.nickName;
    user.avatar = req.body.avatar;
    user.color = req.body.color;

    await user.save(err => {
        if (err) {
            res.send({
                status: "UPDATE_ERROR",
            });
            return;
        }
        res.send({
            status: "UPDATE_SUCCESS",
        });
    }, );
});

module.exports = router;