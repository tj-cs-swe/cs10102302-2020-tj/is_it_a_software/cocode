"use strict";

//主页
const express = require("express");
const router = express.Router();

/* GET home page. */
router.get("/", function(req, res) {
    //res.render('index', { title: 'Express' });

    //主页暂时重定向为测试chat界面
    res.redirect("/chat.html");
    //res.send('hello world');
});

module.exports = router;