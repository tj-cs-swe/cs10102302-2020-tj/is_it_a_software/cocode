"use strict";

const createError = require("http-errors");
const express = require("express");
const unless = require("express-unless");
const path = require("path");
const cookieParser = require("cookie-parser");
const session = require("express-session");
//const MongoStore = require("connect-mongo")(session);
const logger = require("morgan");
const jwt = require("jsonwebtoken");
const { SECRET, SessionId } = require("./secret");
const indexRouter = require("./routes/index");
const usersRouter = require("./routes/users");
const registerRouter = require("./routes/register");
const loginRouter = require("./routes/login");
const profileRouter = require("./routes/profile");
const CreatProjectRouter = require("./routes/creatproject");
const AddMemberRouter = require("./routes/addmember");
const DelProjectsRouter = require("./routes/delproject");
const DelMemberRouter = require("./routes/delmember");
const ShowProjectsRouter = require("./routes/showprojects");
const projectFile = require("./routes/projectfile");
const updateUserRouter = require("./routes/updateUser");
const codeEditRouter = require("./routes/codeEdit");
const projectRouter = require("./routes/project");
const replaceOwnerRouter = require("./routes/replaceowner");
const searchUsernameRouter = require("./routes/searchuser");
const changeDescRouter = require("./routes/changedesc");
//const cm = require("./cocodeManage");

//const { mongooseConn } = require("./db/db");
//const uid = require("uid-safe");
const app = express();

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jade");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(express.static(path.join(__dirname, "public")));

// 自定义跨域中间件
const allowCors = function (req, res, next) {
    res.header("Access-Control-Allow-Origin", req.headers.origin);
    res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS");
    res.header("Access-Control-Allow-Headers", "Content-Type");
    res.header("Access-Control-Allow-Credentials", "true");
    next();
};
app.use(allowCors); //使用跨域中间件

const verifyToken = function (req, res, next) {
    if (!req.headers.authorization) {
        console.log("没有token");
        res.status(403).send(next(createError(403, "缺少token")));
        return;
    }
    const token = String(req.headers.authorization)
        .split(" ")
        .pop();
    jwt.verify(token, SECRET, function (err, decoded) {
        if (err) {
            return res.status(400).send(next(createError(400, "无效的token")));
        } else {
            // 如果验证通过，在req中写入解密结果
            console.log(decoded);
            req.body.userName = decoded.username;
            next();
        }
    });
};
verifyToken.unless = unless;

//验证token
// app.use(
//     verifyToken.unless({
//         path: [
//             { url: "/register", methods: ["GET", "POST", "OPTIONS"] },
//             { url: "/login", methods: ["GET", "POST", "OPTIONS"] },
//         ],
//     }),
// );

// 引入session管理，比authorization token更安全些

const _session = {
    // genid: function() {
    //     return uid(); // use UUIDs for session IDs
    // },
    secret: "cocode", // 用来对session id相关的cookie进行签名
    name: SessionId, // 这里的name指的是cookie的name，默认cookie的name是：connect.sid
    cookie: { maxAge: 80 * 1000 }, // 设置maxAge是80000ms，即80s后session和相应的cookie失效过期
    resave: false, // 是否每次都重新保存会话，建议false
    saveUninitialized: true, // 是否自动保存未初始化的会话，建议false
    //store: new MongoStore({ mongooseConnection: mongooseConn }), // MongoDB存储session
};

if (app.get("env") === "production") {
    app.set("trust proxy", 1); // trust first proxy
    _session.cookie.secure = true; // serve secure cookies
}

app.use(session(_session));

// 这里可以添加根据session的传递，进行的特殊的处理
// Access the session as req.session
app.get("/", function (req, res, next) {
    console.log("get session pass test:   ", req.session, req.session.loginUser);
    next();
});

app.post("/", function (req, res, next) {
    console.log("post session pass test:   ", req.session, req.session.loginUser);
    next();
});

// app.use(function(req, res, next) {
//     if (!req.session) {
//         res.redirect("/");
//         return;
//     }

//     //
//     console.log("session pass test:   ", req.session, req.session.loginUser);
//     // req.body.userName = req.session.loginUser;

//     next();
// });

//在这里添加路由
app.use("/", indexRouter);
app.use("/users", usersRouter);
app.use("/register", registerRouter);
app.use("/login", loginRouter);
app.use("/profile", profileRouter);
app.use("/creatproject", CreatProjectRouter);
app.use("/addmember", AddMemberRouter);
app.use("/delproject", DelProjectsRouter);
app.use("/showprojects", ShowProjectsRouter);
app.use("/delmember", DelMemberRouter);
app.use("/projectfile", projectFile);
app.use("/updateUser", updateUserRouter);
app.use("/codeEdit", codeEditRouter);
app.use("/project", projectRouter);
app.use("/replaceowner", replaceOwnerRouter);
app.use("/searchuser", searchUsernameRouter);
app.use("/changedesc", changeDescRouter);
// catch 404 and forward to error handler
app.use(function (req, res, next) {
    console.log("error");

    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get("env") === "development" ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render("error");
});

module.exports = app;